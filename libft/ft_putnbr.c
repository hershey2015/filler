/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 16:58:12 by coleksii          #+#    #+#             */
/*   Updated: 2016/12/16 17:57:16 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int c)
{
	unsigned int	n;
	char			t;

	n = c;
	if (c < 0)
	{
		ft_putchar('-');
		n = -c;
	}
	else
		n = c;
	if (n <= 9)
	{
		t = n + '0';
		write(1, &t, 1);
	}
	else
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
}
