/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/29 17:17:38 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/06 17:16:33 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

void	correct_int_fig(t_fig *fig)
{
	int	i;
	int	j;
	int	u;

	i = 0;
	j = 0;
	u = 0;
	fig->x1 = (int *)malloc(sizeof(int) * fig->numb);
	fig->y1 = (int *)malloc(sizeof(int) * fig->numb);
	while (i < fig->h)
	{
		j = 0;
		while (j < fig->l)
		{
			if (fig->map[i][j] == '*')
			{
				fig->x1[u] = j;
				fig->y1[u] = i;
				++u;
			}
			j++;
		}
		++i;
	}
}

void	read_fig(t_fig *fig)
{
	int		i;
	char	*s;
	int		j;

	i = 6;
	fig->numb = 0;
	get_next_line(0, &s);
	fig->h = ft_atoi(&s[i]);
	while (s[i] > 47 && s[i] < 58 && s[i] != '\0')
		i++;
	fig->l = ft_atoi(&s[++i]);
	free(s);
	fig->map = (char **)malloc(sizeof(char *) * (fig->h + 1));
	fig->map[fig->h] = NULL;
	i = -1;
	while (++i < fig->h && get_next_line(0, &s))
	{
		j = -1;
		while (s[++j] != '\0')
			if (s[j] == '*')
				(fig->numb)++;
		fig->map[i] = ft_strdup(s);
		free(s);
	}
	correct_int_fig(fig);
}

void	read_all(t_map *cart)
{
	int		i;
	char	*s;

	i = 0;
	cart->map = (char **)malloc(sizeof(char *) * (cart->h + 1));
	cart->map[cart->h] = NULL;
	while (i < cart->h)
	{
		get_next_line(0, &s);
		cart->map[i] = ft_strdup(&s[4]);
		++i;
		free(s);
	}
}

t_map	read_map(t_fig *fig, char *str)
{
	t_map	cart;
	int		i;

	i = 0;
	while ((str[i] > 64 && str[i] < 91) || (str[i] > 96 && str[i] < 123))
		++i;
	while (str[i] == ' ')
		++i;
	cart.h = ft_atoi(&str[i]);
	while (str[i] > 47 && str[i] < 58)
		++i;
	++i;
	cart.l = ft_atoi(&str[i]);
	free(str);
	get_next_line(0, &str);
	free(str);
	read_all(&cart);
	read_fig(fig);
	return (cart);
}

int		read_map_2(t_fig *fig, char *str, t_map *cart)
{
	int	i;

	free(str);
	if (get_next_line(0, &str))
		free(str);
	else
		return (0);
	i = -1;
	while (++i < cart->h)
		free(cart->map[i]);
	free(cart->map);
	read_all(cart);
	i = -1;
	while (++i < fig->h)
		free(fig->map[i]);
	free(fig->x1);
	free(fig->y1);
	free(fig->map);
	read_fig(fig);
	return (0);
}
