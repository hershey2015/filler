/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 15:22:49 by coleksii          #+#    #+#             */
/*   Updated: 2017/06/07 15:48:22 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

void	*ft_memcpy(void *out, const void *in, size_t l)
{
	unsigned char	*o;
	unsigned char	*t;

	o = (unsigned char *)out;
	t = (unsigned char *)in;
	while (l > 0)
	{
		*o++ = *t++;
		l--;
	}
	return (out);
}
