/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/05 18:57:19 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/06 17:51:19 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "head.h"

int		rfirst(char *let)
{
	char	*str;
	int		i;

	i = 0;
	while (i++ < 6)
	{
		get_next_line(0, &str);
		free(str);
	}
	get_next_line(0, &str);
	if (ft_strstr(str, "/filler") && (ft_strstr(str, "p1 : ")))
	{
		*let = 'X';
		free(str);
		return (1);
	}
	return (2);
}

int		time(int t)
{
	int i;

	i = 0;
	while (i < t * 10000000)
		++i;
	return (1);
}

void	put_color_str(char *str, int pl)
{
	if (ft_strstr(str, "got") && write(1, "\n\n\n\n\n", 1) && time(5))
		return ;
	else if (ft_strstr(str, "fin"))
		write(1, str, ft_strlen(str));
	else if (*str > 47 && *str < 57)
	{
		while (*str != '\0')
		{
			if (*str == 'X' || *str == 'x')
				if (pl == 1)
					write(1, "\033[1;31mX\033[0m", 12);
				else
					write(1, "\033[1;32mX\033[0m", 12);
			else if (*str == 'O' || *str == 'o')
				if (pl == 1)
					write(1, "\033[1;32mO\033[0m", 12);
				else
					write(1, "\033[1;31mO\033[0m", 12);
			else if (*str == '.' || *str == '*')
				write(1, str, 1);
			str++;
		}
		write(1, "\n", 1);
	}
}

int		main(void)
{
	char	*str;
	char	let;
	char	pl;

	pl = rfirst(&let);
	while (get_next_line(0, &str))
	{
		put_color_str(str, pl);
		free(str);
	}
	return (0);
}
