/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/11 18:27:43 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/05 19:42:35 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		cycl(t_map *cart, t_fig *f, int *k, int *flag)
{
	k[3] = 0;
	while (k[3] < f->l)
	{
		if (k[1] + k[3] - f->j > cart->l - 1 || k[1] + k[3] - f->j < 0 ||
				k[0] + k[2] - f->i > cart->h - 1 || k[0] + k[2] - f->i < 0)
			return (0);
		if (cart->map[k[0] + k[2] - f->i][k[1] + k[3] - f->j] == cart->let &&
				f->map[k[2]][k[3]] == '*')
			++*flag;
		if (cart->map[k[0] + k[2] - f->i][k[1] + k[3] - f->j] != cart->let &&
				cart->map[k[0] + k[2] - f->i][k[1] + k[3] - f->j] != '.' &&
				f->map[k[2]][k[3]] != '.')
			return (0);
		if (*flag > 1)
			return (0);
		if (f->map[k[2]][k[3]] == '*')
			f->sum = f->sum + cart->copy[k[0] + k[2]
				- f->i][k[1] + k[3] - f->j];
		++k[3];
	}
	return (1);
}

int		putfigure(t_map *cart, t_fig *f, int x, int y)
{
	int i;
	int	j;
	int	k[4];
	int flag;

	flag = 0;
	i = 0;
	k[0] = y;
	k[1] = x;
	k[2] = 0;
	k[3] = 0;
	j = 0;
	f->sum = 0;
	while (k[2] < f->h)
	{
		if (k[0] + k[2] - f->i > cart->h || k[0] + k[2] - f->i < 0)
			return (0);
		if (cycl(cart, f, k, &flag) == 0)
			return (0);
		++k[2];
	}
	if (flag != 1)
		return (0);
	return (1);
}

int		get_figure(t_map *cart, t_fig *fig, int x, int y)
{
	int		i;
	t_fig	f;
	int		u;

	f = *fig;
	u = 0;
	i = 0;
	while (i < fig->numb)
	{
		f.i = fig->y1[i];
		f.j = fig->x1[i];
		if (putfigure(cart, &f, x, y) && f.sum > fig->sum)
		{
			u = 1;
			fig->sum = f.sum;
			fig->x = x - f.j;
			fig->y = y - f.i;
		}
		i++;
	}
	if (u)
		return (1);
	return (0);
}

void	print(t_fig *fig)
{
	char	*s;

	s = ft_itoa(fig->y);
	write(1, s, ft_strlen(s));
	free(s);
	s = ft_itoa(fig->x);
	write(1, " ", 1);
	write(1, s, ft_strlen(s));
	write(1, "\n", 1);
	free(s);
}

int		pars(t_map *cart, t_fig *fig, int pl)
{
	int		x;
	int		y;
	int		u;

	fig->sum = 0;
	u = 0;
	cart->let = (pl == 1) ? 'O' : 'X';
	y = -1;
	while (++y < cart->h)
	{
		x = -1;
		while (++x < cart->l)
			if (cart->map[y][x] == cart->let)
				if ((y < cart->h))
					if (get_figure(cart, fig, x, y))
						u = 1;
	}
	if (!u && write(1, "0 0\n", 4))
		return (-1);
	print(fig);
	return (0);
}
