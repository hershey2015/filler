/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analiz.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 14:33:26 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/05 17:58:45 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		correct_int_map(t_map *cart, int y, int x, int ind)
{
	int		i;
	int		j;
	int		l;
	int		h;
	int		k;

	i = 0;
	j = 0;
	while (i < cart->h)
	{
		j = 0;
		while (j < cart->l)
		{
			l = (x - j > 0) ? x - j : j - x;
			h = (y - i > 0) ? y - i : i - y;
			k = ind - h - l;
			cart->copy[i][j] = k > cart->copy[i][j] ? k : cart->copy[i][j];
			++j;
		}
		++i;
	}
	return (0);
}

int		contin(t_map *cart, int ind)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	while (y < cart->h)
	{
		x = 0;
		while (x < cart->l)
		{
			if (cart->copy[y][x] == ind)
				correct_int_map(cart, y, x, ind);
			++x;
		}
		++y;
	}
	return (0);
}

int		analiz(t_map *cart, char let)
{
	char	**s;
	int		i;
	int		j;
	int		ind;

	i = 0;
	s = cart->map;
	ind = (cart->h > cart->l) ? cart->h * 2 : cart->l * 2;
	cart->copy = (int **)malloc(sizeof(int *) * (cart->h + 1));
	while (i < cart->h)
	{
		j = 0;
		cart->copy[i] = (int *)malloc(sizeof(int) * (cart->l + 1));
		while (j < cart->l)
		{
			if (s[i][j] == let)
				cart->copy[i][j] = ind;
			else
				cart->copy[i][j] = 0;
			++j;
		}
		++i;
	}
	contin(cart, ind);
	return (0);
}

int		contin2(t_map *cart, int ind)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	while (y < cart->h)
	{
		x = 0;
		while (x < cart->l)
		{
			if (cart->copy[y][x] == ind + 1)
			{
				cart->copy[y][x] -= 1;
				correct_int_map(cart, y, x, ind);
			}
			++x;
		}
		++y;
	}
	return (0);
}

int		analiz_2(t_map *cart, char let)
{
	char	**s;
	int		i;
	int		j;
	int		ind;

	i = 0;
	s = cart->map;
	ind = (cart->h > cart->l) ? cart->h * 2 : cart->l * 2;
	while (i < cart->h)
	{
		j = 0;
		while (j < cart->l)
		{
			if (s[i][j] == let && cart->copy[i][j] < ind)
				cart->copy[i][j] = ind + 1;
			++j;
		}
		++i;
	}
	contin2(cart, ind);
	return (0);
}
