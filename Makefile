# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: coleksii <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/28 15:27:44 by coleksii          #+#    #+#              #
#    Updated: 2017/07/06 17:54:37 by coleksii         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = @gcc

NAME = filler

OBJ = faller.o ft_strjoin.o get_next_line.o ft_strlen.o ft_strrchr.o ft_lstadd.o ft_lstnew.o ft_strdup.o ft_strchr.o ft_memcpy.o ft_strstr.o ft_atoi.o analiz.o pars.o read.o ft_itoa.o

CFLAGS = -Wall -Wextra -Werror

HEADER = head.h

all: $(NAME)

$(NAME): $(OBJ)
	@gcc -o $(NAME) -I $(HEADER) $(OBJ)
	@echo "\033[32;1m<<done>>\033[0m"
clean:
	@rm -f $(OBJ)
	@echo "\033[32;1m<<delete objects>>\033[0m"

fclean: clean
	@rm -rf $(NAME)
	@echo "\033[32;1m<<delete fdf>>\033[0m"

re: fclean all
	@echo "\033[32;1m<<re succes>>\033[0m"

