/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   faller.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 17:53:49 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/06 17:50:41 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int	read_first(char *let)
{
	char	*str;

	get_next_line(0, &str);
	if (ft_strstr(str, "/filler") && (ft_strstr(str, "p1 : ")))
	{
		*let = 'X';
		free(str);
		return (1);
	}
	if (ft_strstr(str, "/filler") && (ft_strstr(str, "p2 : ")))
	{
		*let = 'O';
		free(str);
		return (2);
	}
	return (0);
}

int	clean_all(t_map *cart, t_fig *fig)
{
	int i;

	i = -1;
	while (++i < cart->h)
		free(cart->map[i]);
	free(cart->map);
	i = -1;
	while (++i < fig->h)
		free(fig->map[i]);
	free(fig->x1);
	free(fig->y1);
	free(fig->map);
	i = -1;
	while (++i < cart->h)
		free(cart->copy[i]);
	free(cart->copy);
	return (1);
}

int	main(void)
{
	int		pl;
	char	let;
	t_map	cart;
	t_fig	fig;
	char	(*str);

	pl = read_first(&let);
	get_next_line(0, &str);
	cart = read_map(&fig, str);
	analiz(&cart, let);
	if (pars(&cart, &fig, pl) == -1)
		return (0);
	while (1)
	{
		if (get_next_line(0, &str))
		{
			read_map_2(&fig, str, &cart);
			analiz_2(&cart, let);
			if (pars(&cart, &fig, pl) == -1 && clean_all(&cart, &fig))
				return (0);
		}
	}
	return (0);
}
