/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   head.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 14:01:38 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/06 15:08:21 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEAD_H
# define HEAD_H
# define BUFF_SIZE 1

# include <stdio.h>
# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;
typedef struct	s_map
{
	char		**map;
	int			**copy;
	int			h;
	int			l;
	char		let;
}				t_map;
typedef struct	s_f
{
	char		**map;
	int			h;
	int			l;
	int			x;
	int			y;
	int			sum;
	int			i;
	int			j;
	int			numb;
	int			*x1;
	int			*y1;
}				t_fig;
char			*ft_strjoin(char const *s1, char const *s2);
size_t			ft_strlen(const char *s);
char			*ft_strrchr(const char *s, int c);
int				get_next_line(const int fd, char **line);
char			*ft_strdup(const char *s1);
char			*ft_strchr(const char *s, int c);
void			*ft_memcpy(void *out, const void *in, size_t l);
void			ft_lstadd(t_list **alst, t_list *n);
t_list			*ft_lstnew(void const *content, size_t content_size);
char			*ft_strstr(const char *big, const char *lit);
int				ft_atoi(const char *str);
int				analiz(t_map *cart, char let);
int				analiz_2(t_map *cart, char let);
int				pars(t_map *car, t_fig *fig, int pl);
int				read_map_2(t_fig *fig, char *str, t_map *cart);
t_map			read_map(t_fig *fig, char *str);
char			*ft_itoa(int n);

#endif
